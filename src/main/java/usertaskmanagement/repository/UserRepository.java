package usertaskmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import usertaskmanagement.entity.Users;

/**
 * <h1>User Repository</h1>
 * This Class forms the database call methods that use the user entity.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

	List<Users> findByUsername(@Param("username") String username);

}