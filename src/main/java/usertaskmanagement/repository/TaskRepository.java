package usertaskmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import usertaskmanagement.entity.Tasks;

/**
 * <h1>Task Repository</h1>
 * This Class forms the database call methods that use the task entity.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@Repository
public interface TaskRepository extends JpaRepository<Tasks, Long>, TaskRepositoryCustom {

	List<Tasks> findByUserId(@Param("user_id") long user_id);
	
}