package usertaskmanagement.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import usertaskmanagement.TaskStatusEnum;
import usertaskmanagement.entity.Tasks;

/**
 * <h1>Task Repository Custom</h1>
 * This Class forms the custom database calls methods that use the task entity.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@Repository
@Transactional(readOnly = true)
class TaskRepositoryImpl implements TaskRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    /**
     * This method runs a custom query to retrieve tasks that are over due.
     * 
     * @param TaskStatusEnum
     * @return List<Tasks>
     */
	@Override
	public List<Tasks> findByStatusAndTime(TaskStatusEnum taskStateEnum) {
        
        Query query = entityManager.createNativeQuery("SELECT * FROM Tasks WHERE Tasks.status = ? AND NOW() > Tasks.date_time", Tasks.class);
        query.setParameter(1, taskStateEnum.name());

        return query.getResultList();

	}
}