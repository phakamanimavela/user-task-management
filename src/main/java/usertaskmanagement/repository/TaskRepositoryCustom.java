package usertaskmanagement.repository;

import java.util.List;

import usertaskmanagement.TaskStatusEnum;
import usertaskmanagement.entity.Tasks;

/**
 * <h1>Task Repository Custom</h1>
 * This Class forms the custom database calls methods that use the task entity.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
public interface TaskRepositoryCustom {

    List<Tasks> findByStatusAndTime(TaskStatusEnum taskStateEnum);
    
}