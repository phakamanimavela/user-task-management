package usertaskmanagement.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * <h1>User Not Found Exception</h1>
 * This Exception is thrown when a user cannot be found.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	/**
	 * This method sets the exception message using {@literal id}.
	 * 
	 * @param id
	 */
	public UserNotFoundException(Long userId) {
		super("could not find user '" + userId + "'.");
	}
}