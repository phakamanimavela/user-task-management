package usertaskmanagement.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * <h1>Task Not Found Exception</h1>
 * This Exception is thrown when a taks cannot be found.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class TaskNotFoundException extends RuntimeException {

	/**
	 * This method sets the exception message using {@literal id}.
	 * 
	 * @param id
	 */
	public TaskNotFoundException(long id) {
		super("could not find task '" + id + "'.");
	}
}