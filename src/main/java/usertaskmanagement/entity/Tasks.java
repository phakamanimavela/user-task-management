package usertaskmanagement.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

import usertaskmanagement.TaskStatusEnum;

/**
 * <h1>Task Entity</h1>
 * This entity forms part of the persistence layer for task api calls.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@Entity
public class Tasks {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private long   userId;
	
	private String name;

	private String description;

	@Enumerated(value = EnumType.STRING)
	private TaskStatusEnum status;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp date_time;

	public long getId()
	{
		return id;
	}

	/**
	 * @return the status
	 */
	public TaskStatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(TaskStatusEnum status) {
		this.status = status;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the date_time
	 */
	public Timestamp getDate_time() {
		return date_time;
	}

	/**
	 * @param date_time the date_time to set
	 */
	public void setDate_time(Timestamp date_time) {
		this.date_time = date_time;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Task = {"+"id : "+ getId()+
					", userId : "+ getUserId()+
					", name : "+ getName() +
					", description : "+getDescription() +
					", status : "+ getStatus() +
					",date_time : "+getDate_time()+
					"}";
	}

}