package usertaskmanagement.contoller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import usertaskmanagement.entity.Users;
import usertaskmanagement.repository.UserRepository;

/**
 * <h1>Task Rest Controller</h1>
 * This REST contoller manages the creation, listing and updating, deletion of Tasks.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@RestController
public class UserRestController {

    private static final String API_USER_ID = "/api/user/{id}";
    private static final String API_USER = "api/user";
    
	private final UserRepository userRepository;
    
    /**
     * This method is used to intantiate the required repositories for this controller.
     * 
     * @param userRepository
     * @param taskRepository
     */
    @Autowired
    UserRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method creates a user and returns a ResponseBody to the consumer.
     * 
     * @param user_id
     * @param tak
     * @return ResponseEntity<?>
     */
    @PostMapping(API_USER)
	ResponseEntity<Object> createUser(@RequestBody Users user) {
        
        Users storedUser = userRepository.saveAndFlush(user);

        URI location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(storedUser.getId())
            .toUri();

        return ResponseEntity.created(location).body(storedUser);    
    
    }

    /**
     * This method updates a user given the {@literal id} of
     * the task matches what has previously been persisted.
     * 
     * @param id
     * @return ResponseEntity
     */
    @PutMapping(API_USER_ID)
    public ResponseEntity<?> updateUser(@RequestBody Users user, @PathVariable long id) {

        Optional<Users> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent())
            return ResponseEntity.notFound().build();

        if (user.getUsername() != null)    
            userOptional.get().setUsername(user.getUsername());

        if (user.getFirst_name() != null)    
            userOptional.get().setFirst_name(user.getFirst_name());
        
        if (user.getLast_name() != null)    
            userOptional.get().setLast_name(user.getLast_name());        
        
        userRepository.save(userOptional.get());

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(userOptional.get());
    }

    /**
     * This method retrieves a specific user {@literal id}.
     * 
     * @param id
     * @return Users
     */
    @GetMapping(API_USER_ID)
    public Users retrieveUser(@PathVariable long id) {
        Optional<Users> user = userRepository.findById(id);

        return user.get();
    }

    /**
     * This method retrieves all tasks created.
     * @return List<UserRepository>
     */
    @GetMapping(API_USER)
    public List<Users> retrieveAllUsers() {
        return userRepository.findAll();
    }
}