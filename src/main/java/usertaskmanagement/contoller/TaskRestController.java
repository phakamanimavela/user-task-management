package usertaskmanagement.contoller;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import usertaskmanagement.TaskStatusEnum;
import usertaskmanagement.entity.Tasks;
import usertaskmanagement.repository.TaskRepository;
import usertaskmanagement.repository.UserRepository;
import usertaskmanagement.exceptions.*;

/**
 * <h1>Task Rest Controller</h1>
 * This REST contoller manages the creation, listing, updating and deleting of tasks.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@RestController
@RequestMapping("api/user/{user_id}")
class TaskRestController {

    private static final String USER_ID_TASK = "/task";
    private static final String USER_ID_TASK_ID = "/task/{id}";
        
    private final UserRepository userRepository;
	private final TaskRepository taskRepository;
    
    /**
     * This method is used to intantiate the required repositories for this controller.
     * 
     * @param userRepository
     * @param taskRepository
     */
    @Autowired
    TaskRestController(UserRepository userRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * This method creates a task and returns a ResponseBody to the consumer.
     * 
     * @param user_id
     * @param tak
     * @return ResponseEntity<?>
     */
    @PostMapping(USER_ID_TASK)
	ResponseEntity<?> createTask(@RequestBody Tasks task, @PathVariable long user_id) {

        // validate user id exists
        this.validateUserId(user_id);

        // add valid user id to task
        task.setUserId(user_id);

        // add default taks status
        task.setStatus(TaskStatusEnum.PENDING);

        Tasks storedTask = taskRepository.save(task);

        URI location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{user_id}")
            .buildAndExpand(storedTask.getId())
            .toUri();

        return ResponseEntity.created(location).body(storedTask);    
    
    }

    /**
     * This method updates a task given the {@literal user_id} and {@literal id}
     * of the task matches what has previously been persisted.
     * 
     * @param task
     * @param user_id
     * @param id
     * @return ResponseEntity<?>
     */
    @PutMapping(USER_ID_TASK_ID)
    public ResponseEntity<?> updateTask(@RequestBody Tasks task, @PathVariable long user_id, @PathVariable long id) {

        // validate user id exists
        this.validateUserId(user_id);

        Optional<Tasks> taskOptional = taskRepository.findById(id);

        if (!taskOptional.isPresent())
            return ResponseEntity.notFound().build();

        if (task.getName() != null)    
            taskOptional.get().setName(task.getName());

        if (task.getDescription() != null)    
            taskOptional.get().setDescription(task.getDescription());
        
        if (task.getStatus() != null)    
            taskOptional.get().setStatus(task.getStatus());    

        if (task.getDate_time() != null)    
            taskOptional.get().setDate_time(task.getDate_time());    
        
        taskRepository.save(taskOptional.get());

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(taskOptional.get());
    }

    /**
     * This method deletes a task given the {@literal} and {@literal id}
     * of the task matches what has previously been persisted.
     * 
     * @param user_id
     * @param id
     * @return ResponseEntity<?>
     */
    @DeleteMapping(USER_ID_TASK_ID)
    public ResponseEntity<?> deleteTask(@PathVariable long user_id, @PathVariable long id) {

        // validate user id exists
        this.validateUserId(user_id);
        
        taskRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    /**
     * This method retrieves a specific task, {@literal id} created by a user {@literal user_id}.
     * 
     * @param user_id
     * @param id
     * @return Tasks
     */
    @GetMapping(USER_ID_TASK_ID)
    public Tasks retrieveTask(@PathVariable long user_id, @PathVariable long id) {

        // validate user id exists
        this.validateUserId(user_id);

        return this.taskRepository
            .findById(id)
            .orElseThrow(() -> new TaskNotFoundException(id));
    }

    /**
     * This method retrieves all tasks created by a user {@literal}.
     * 
     * @param user_id
     * @return Collection<Tasks>
     */
    @GetMapping(USER_ID_TASK)
    public Collection<Tasks> listAllTasksForUser(@PathVariable long user_id) {
        
        return this.taskRepository.findByUserId(user_id);
    
    }

    /**
	 * Verify the {@literal userId} exists.
	 *
	 * @param userId
     * @return UserRepository
	 */
	private void validateUserId(Long userId) {
		this.userRepository
			.findById(userId)
			.orElseThrow(()-> new UserNotFoundException(userId));
	}

}