package usertaskmanagement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import usertaskmanagement.entity.Tasks;
import usertaskmanagement.repository.*;

/**
 * <h1>Scheduled Task Check</h1>
 * This class run a scheduled task to check all over due tasks.
 * 
 * @author Phakamani I. Mavela (phakamanimavela@gmail.com)
 * @version 0.1
 */
@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private final TaskRepository taskRepository;

    /**
     * 
     */
    ScheduledTasks(final TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    /**
     * 
     */
    @Scheduled(fixedRate = 10000)
    public void updateOverdueTasks() {

        log.info("Checking for overdue tasks");

        List<Tasks> tasks = taskRepository.findByStatusAndTime(TaskStatusEnum.PENDING);

        if (tasks.size() > 0)
        {
            for (Tasks task : tasks) {

                task.setStatus(TaskStatusEnum.DONE);
    
                taskRepository.save(task);
    
                log.info("Move task {} to done", task.getId());
    
                log.info(task.toString());
            }    
        }
    }
}