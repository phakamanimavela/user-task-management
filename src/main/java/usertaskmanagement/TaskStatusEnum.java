package usertaskmanagement;

public enum TaskStatusEnum {
    PENDING,
    DONE
}