------ Default Users to add tasks for -------
---------------------------------------------
INSERT INTO Users(username,first_name,last_name)
VALUES('B_Baggins','Bilbo','Baggins');

INSERT INTO Users(username,first_name,last_name)
VALUES('F_Baggins','Frodo', 'Baggins');

INSERT INTO Users(username,first_name,last_name)
VALUES('G_The_Gray','Gandulf','the Gray');

---- Default Tasks based on above users -----
---------------------------------------------

INSERT INTO Tasks(user_id,name,description,status,date_time)
VALUES(1,'this is the first task im adding','first task','PENDING','2018-08-02 17:25:00');

INSERT INTO Tasks(user_id,name,description,status,date_time)
VALUES(2,'this is the second task im adding','second task','DONE','2018-08-02 17:25:00');

INSERT INTO Tasks(user_id,name,description,status,date_time)
VALUES(3,'this is the third task im adding','third task','PENDING','2018-08-02 17:25:00');