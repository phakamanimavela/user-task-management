package usertaskmanagement;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import usertaskmanagement.entity.Tasks;
import usertaskmanagement.repository.TaskRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepositoryTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private TaskRepository taskRepository;
 
    // write test cases here
    @Test
    public void whenFindByName_thenReturnEmployee() {
        // given
        Tasks task = new Tasks();

        task.setUserId(100L);
        task.setName("test task");
        task.setDescription("mavela");
        task.setStatus(TaskStatusEnum.PENDING);

        entityManager.persist(task);
        entityManager.flush();
    
        // when
        Tasks found = taskRepository.findByUserId(task.getUserId()).get(0);
    
        // then
        assertThat(found.getUserId())
        .isEqualTo(task.getUserId());

        assertThat(found.getName())
        .isEqualTo(task.getName());

        assertThat(found.getDescription())
        .isEqualTo(task.getDescription());
    }
 
}