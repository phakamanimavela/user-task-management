package usertaskmanagement;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import usertaskmanagement.entity.Users;
import usertaskmanagement.repository.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private UserRepository userRepository;
 
    // write test cases here
    @Test
    public void whenFindByUsername_thenReturnUser() {
        // given
        Users user = new Users();

        user.setUsername("pmavela");
        user.setFirst_name("phakamani");
        user.setLast_name("mavela");

        entityManager.persist(user);
        entityManager.flush();
    
        // when
        Users found = userRepository.findByUsername(user.getUsername()).get(0);
    
        // then
        assertThat(found.getUsername())
        .isEqualTo(user.getUsername());

        assertThat(found.getFirst_name())
        .isEqualTo(user.getFirst_name());

        assertThat(found.getLast_name())
        .isEqualTo(user.getLast_name());
    }
 
}